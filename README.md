# gatling-project
Using Gatling or JMeter tool script the next flow of following site:
- Open homepage http://demo.nopcommerce.com/
- Search for random product (with autocomplete requests)
- Open random product page from search results
- Add to cart
- Go to shopping cart

Add specific assertions for each request (e.g. response codes, response body etc.)
Turn off “Follow Redirect” on each request.
