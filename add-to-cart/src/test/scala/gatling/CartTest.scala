package gatling

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.fetch.HtmlParser
import io.gatling.jdbc.Predef._

import scala.util.Random

class CartTest extends Simulation {

  val httpProtocol = http
    .baseUrl("https://demo.nopcommerce.com")
    .inferHtmlResources()
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("uk-UA,uk;q=0.9,ru;q=0.8,en-US;q=0.7,en;q=0.6,bg;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36")
    .disableFollowRedirect

  val headers_0 = Map(
    "sec-fetch-dest" -> "document",
    "sec-fetch-mode" -> "navigate",
    "sec-fetch-site" -> "none",
    "sec-fetch-user" -> "?1",
    "upgrade-insecure-requests" -> "1")

  val headers_1 = Map(
    "accept" -> "application/json, text/javascript, */*; q=0.01",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "cors",
    "sec-fetch-site" -> "same-origin",
    "x-requested-with" -> "XMLHttpRequest")

  val headers_2 = Map(
    "sec-fetch-dest" -> "document",
    "sec-fetch-mode" -> "navigate",
    "sec-fetch-site" -> "same-origin",
    "sec-fetch-user" -> "?1",
    "upgrade-insecure-requests" -> "1")

  val headers_3 = Map(
    "Accept" -> "*/*",
    "Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
    "Origin" -> "https://demo.nopcommerce.com",
    "Proxy-Connection" -> "keep-alive",
    "X-Requested-With" -> "XMLHttpRequest")

  val keywords = Seq("lenovo", "htc")


  val scn = scenario("CartTest")
    .exec(http("homePage")
      .get("/")
      .check(regex("<title>(.+?)<\\/title>").is("nopCommerce demo store"))
      .headers(headers_0))
    .pause(4)
    .exec(session => {
      val searchWord = keywords(Random.nextInt(keywords.length))
      session
        .set("searchWord", searchWord)
        .set("searchUrl", "/catalog/searchtermautocomplete?term=" + searchWord)
    })
    .exec(http("searchProduct")
      .get("${searchUrl}")
      .check(jsonPath("$..producturl").findRandom(1).transform(seq => seq.head).saveAs("productUrl"))
      .check(regex("${searchWord}").exists)
      .check(status.is(200))
      .headers(headers_1))
    .pause(4)
    .exec(http("chooseProduct")
      .get("${productUrl}")
      .headers(headers_2)
      .check(
        css("input[data-productid]", "data-productid").find.saveAs("productId"),
        css("input[name=\"__RequestVerificationToken\"]", "value").find.saveAs("requestVerificationToken")
      )
      .check(css("#product-details-form", "action").is("${productUrl}"))
    )
    .pause(4)
    .exec(
      http("AddToCart")
        .post("/addproducttocart/details/${productId}/1")
        .headers(headers_3)
        .formParam("addtocart_${productId}.EnteredQuantity", "1")
        .formParam("CountryId", "0")
        .formParam("StateProvinceId", "0")
        .formParam("ZipPostalCode", "")
        .formParam("__RequestVerificationToken", "${requestVerificationToken}")
        .check(jsonPath("$.message").is("The product has been added to your <a href=\\\"/cart\\\">shopping cart</a>"))
    )
    .exec(http("cartPage")
      .get("/cart")
      .headers(headers_2)
      .check(regex("Your Shopping Cart is empty!").notExists)
      .check(css("div[class=\"product\"] a", "href").is("${productUrl}"))
    )

  setUp(scn.inject(rampUsers(3) during (5 seconds))).assertions(
    global.responseTime.max.lt(1500),
    global.successfulRequests.percent.gt(95)
  ) protocols (httpProtocol)
}